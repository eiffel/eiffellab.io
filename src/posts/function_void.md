---
title: "Function without arguments"
date: "2020-05-13"
path: "/function-void"
author: "Eiffel"
excerpt: "In this post, we will see that it is possible to pass argument to function without arguments"
tags: ["C", "code"]
---
In C, it is possible to define a function without specifying its argument like `void foo()`.
On a first sight, one would think that this function does not take any argument.
In fact, it can take any argument and the following code highlights this behavior:

```c
#include <stdlib.h>
#include <stdio.h>


void foo(){
	int val;

	asm volatile(
		/*
		 * By default, gcc uses AT&T assembler syntax where instructions run like
		 * that:
		 * instruction src, dst
		 * Also, register are prefixed with "%" and with inline asm we need to write
		 * "%%" to print a single "%".
		 * With the System V AMD64 ABI, rdi holds the first function argument.
		 */
		"mov %%edi, %0"
		: "=r" (val) // Output will be stored in val.
		: // There is not input.
		: // And the code does not modify any register.
	);

	printf("%d\n", val);
}

int main(void){
	foo(42);

	return EXIT_SUCCESS;
}
```

Compiling the above code does not produce errors since it is possible to give arguments to functions without defining their arguments.
So, in this code, `main` calls `foo` with argument 42.
Inside `foo`, I get this value by using [inline assembly](https://gcc.gnu.org/onlinedocs/gcc/Extended-Asm.html) and reading register `edi`.
Indeed, with System V AMD64 ABI ([see Figure 3.4](https://github.com/hjl-tools/x86-psABI/wiki/x86-64-psABI-1.0.pdf)), this register contains the first function argument.
The code effectively prints the value 42 on standard output.

Now, if we correct the function definition with `void foo(void)`, it will not compile anymore with the following error:

> error: too many arguments to function ‘foo’

To conclude, if you want to define a function which does not take any arguments, defines it with `void` as its argument.