---
title: "Hello, world"
date: "2020-05-12"
path: "/hello-world"
author: "Eiffel"
excerpt: "Hello, world"
tags: ["C", "code"]
---
## Hello and welcome to my blog!

It will be dedicated to computer science so here is a *hello world* to start it:

```c
#include <stdio.h>
#include <stdlib.h>

int main(void){
	printf("hello, world\n");

	return EXIT_SUCCESS;
}
```

## "hello, world"?

But why do we use the string "hello world" to illustrate the basic syntax of a programming language?

In fact, the first program example of [The C Programming Language book](https://en.wikipedia.org/wiki/The_C_Programming_Language) prints the string "hello, world" to the standard output.

So, printing this string became *de facto* the classical example to highlight a programming language's syntax!

## EXIT_SUCCESS?

Instead of using `return 0;` to signal that my program exits successfully I rather use `EXIT_SUCCESS` macro. This macro (and its opposite `EXIT_FAILURE`) makes the code system independent.

Indeed, for some operating system (Virtual Memory System according to [this post](https://stackoverflow.com/a/8868139)) the `return 1;` signals the program exits correctly.