---
title: "About"
date: "2020-05-12"
author: "Eiffel"
path: "/about"
---

## About me

I am a French computer scientist.

I am interested in kernel programming, hardware and embedded systems.

Abuot the name of the blog and my pseudonym, I choose *eiffel* because it is the pronunciation of my initials.

## About the blog

My goal here is to share what I learn, to do so, I try to be as pedagogic as I can.

If something is wrong here, or if you want some other information, you can open an issue [here](https://gitlab.com/eiffel/eiffel.gitlab.io/-/issues).